<?php

// chargement de la partie autoload (voir composer.json pour la définition du namespace)
// https://getcomposer.org/doc/04-schema.md#psr-4

use Twig\Loader\FilesystemLoader;

require_once 'vendor/autoload.php';
// ne pas oublier de faire un composer update pour charger les changmenets de mon composer.json

// on specifie où se trouve nos vues 
$loader = new FilesystemLoader('src/view');

// on cree une instance de twig qu'on passera à nos controllers 
$twig = new \Twig\Environment($loader); 

// ici on recupere le nom du controller qui sera appelé par defaut (Default)
$controllerName = isset($_GET["controller"]) ? $_GET["controller"] : "Default";

// ici on recupere le nom de la methode du controller qui sera appelée par defaut (home) 
$method = isset($_GET["method"]) ? $_GET["method"] : "home";

//on instancie le controller et on appelle la methode 
$controllerClassName
    = "TwigDemo\\controller\\" . $controllerName . "Controller";
$controller = new $controllerClassName($twig);

$controller->$method();
