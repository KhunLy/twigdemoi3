<?php


namespace TwigDemo\model;


use TwigDemo\exception\ValidationException;

class Product
{
    private $id;
    private $name;
    private $description;
    private $price;
    private $discount;
    private $image;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     * @throws ValidationException
     */
    public function setName($name)
    {
        if (!$name) {
            throw new ValidationException("This field is required");
        }
        if (strlen($name) > 50 && strlen($name) < 3){
            throw new ValidationException("This field length must be between 3 and 50");
        }
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Product
     * @throws ValidationException
     */
    public function setPrice($price)
    {
        if(!$price){
            throw new ValidationException("This field is required");
        }
        if(!filter_var($price, FILTER_VALIDATE_FLOAT)){
            throw new ValidationException("This field must be a numerical value");
        }
        if(floatval($price) <= 0) {
            throw new ValidationException("This field must be greater than 0");
        }
        $this->price = floatval($price);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return Product
     * @throws ValidationException
     */
    public function setDiscount($discount)
    {
        if(!filter_var($discount, FILTER_VALIDATE_INT)) {
            throw new ValidationException("This field must be a number");
        }
        if(intval($discount) > 50)
        {
            throw  new ValidationException("this field must lower than 50");
        }
        $this->discount = intval($discount);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }


    public function getPriceWithDiscount() {
        return $this->price
            - ($this->price * ($this->discount ?: 0) / 100);
    }


}