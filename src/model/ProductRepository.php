<?php


namespace TwigDemo\model;


class ProductRepository
{
    private $pdo;
    public function __construct()
    {
        $port = 3306;
        $dbname = "twigdemo";
        $host = "localhost";
        $this->pdo = new \PDO("mysql:host=$host;port=$port;dbname=$dbname", "root", null);
    }

    public function insert(Product $product)
    {
        $query = "INSERT INTO product(id, name, description, price, discount, image)
            VALUES(null, ?, ?, ?, ?, ?)";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([
            $product->getName(),
            $product->getDescription(),
            $product->getPrice(),
            $product->getDiscount(),
            $product->getImage()
        ]);
    }

    /**
     * @return Product[]
     */
    public function getAll()
    {
        $query = "SELECT * FROM product";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_CLASS, Product::class);
    }

    public function delete($id)
    {
        $query = "DELETE FROM product WHERE id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([$id]);
    }

    /**
     * @param $id
     * @return Product
     */
    public function get($id)
    {
        $query = "SELECT * FROM product WHERE id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([$id]);
        return $stmt->fetchObject(Product::class);
    }

    public function update(Product $p)
    {
        $query = "UPDATE product 
            SET name = ?, 
                description = ?, 
                price = ?, 
                discount = ?, 
                image = ? 
            WHERE id = ?";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([
            $p->getName(),
            $p->getDescription(),
            $p->getPrice(),
            $p->getDiscount(),
            $p->getImage(),
            $p->getId()
        ]);
    }
}