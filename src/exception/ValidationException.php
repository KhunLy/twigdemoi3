<?php


namespace TwigDemo\exception;


use Throwable;

class ValidationException extends \Exception
{
    public function __construct(
        $message = "", $code = 0, Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
        // je fais ce que je dois faire ...
    }
}