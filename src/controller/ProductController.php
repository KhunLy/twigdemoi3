<?php


namespace TwigDemo\controller;


use TwigDemo\model\Product;
use TwigDemo\model\ProductRepository;

class ProductController extends BaseController
{
    // ?controller=product&method=index
    public function index ()
    {
        $repo = new ProductRepository();
        $list = $repo->getAll();
        $this->render("Product/index.html.twig", [
            "products" => $list
        ]);
    }

    // ?controller=product&method=create
    public function create()
    {
        if($_SERVER["REQUEST_METHOD"] === "POST") {
            $p = new Product();
            $errors = $this->handleRequest($p);
            if(count($errors) == 0) {
                $repo = new ProductRepository();
                $repo->insert($p);
                // afficher un message de success
                header("Location: http://localhost/twigdemoi3/?controller=product&method=index");
            }
            // sinon
            else {
                $this->render("Product/create.html.twig", [
                    "product" => $p,
                    "errors" => $errors
                ]);
            }
        }
        else{
            $this->render("Product/create.html.twig");
        }
    }

    // ?controller=product&method=delete&id=42
    public function delete()
    {
        $id = $_GET["id"];
        $repo = new ProductRepository();
        $repo->delete($id);
        header("Location: http://localhost/twigdemoi3/?controller=product&method=index");
    }
    // ?controller=product&method=update&id=42
    public function update()
    {
        $id = $_GET["id"];
        $repo = new ProductRepository();
        $prod = $repo->get($id);
        if($_SERVER["REQUEST_METHOD"] === "POST"){
            $errors = $this->handleRequest($prod);
            if(count($errors) == 0) {
                $repo->update($prod);
                // afficher un message de success
                header("Location: http://localhost/twigdemoi3/?controller=product&method=index");
            }
            else {
                $this->render("Product/create.html.twig", [
                    "product" => $prod,
                    "errors" => $errors
                ]);
            }
        }
        else{
            $this->render("Product/create.html.twig", [
                "product" => $prod
            ]);
        }
    }
    // ?controller=product&method=details&id=42
    public function details()
    {

    }
}