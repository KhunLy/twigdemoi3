<?php

namespace TwigDemo\controller;

use TwigDemo\exception\ValidationException;

 class BaseController{
    private $twig; 

    public function __construct($twig){
        $this->twig = $twig; 
    }

    protected function render($view, $parameters = []){
        echo $this->twig->render($view, $parameters); 
    }

    protected function handleRequest($object) {
        $errors = [];
        foreach ($_POST as $key => $value){
            try {
                $methodName = "set" . ucfirst($key);
                if (method_exists($object, $methodName)) {
                    $object->$methodName($value);
                }
            } catch (ValidationException $e) {
                $errors[$key] = $e->getMessage();
            }
        }
        return $errors;
    }

}