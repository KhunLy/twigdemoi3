<?php

namespace TwigDemo\controller; 

use \DateTime;
use TwigDemo\model\Product;

class DefaultController extends BaseController{
    public function home(){
        $var = "valeur"; 
        $this->render("Default/home.html.twig",[
            "nomDeLaVariableDansTwig" => $var
        ]);
    }

    public function filters(){
        $chaine = "Une chaine de caractères"; 
        $date = new DateTime('1970-01-01');
        $nombre = 3.14159; 
        $this->render("Default/filters.html.twig", [
            "chaine" => $chaine, 
            "date" => $date, 
            "nombre" => $nombre
        ]); 
    }

    public function loops(){
        $this->render("Default/loops.html.twig",[
            "liste" => ['Pomme','Poire','Orange']
        ]);
    }

    public function model() {
        $p = new Product();
        $p->setName("Coca Cola")
            ->setPrice(10)
            ->setDescription("Boisson au gout de cola")
            ->setDiscount(10);
        $this->render("Default/model.html.twig", [
            'product' => $p
        ]);
    }

    public function heritage() {
        $this->render("Default/heritage.html.twig");
    }
}